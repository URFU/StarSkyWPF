﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace StarSky
{
    class BlinkSky
    {
        Canvas BackgroundSky;
        public bool flagStop = false;
        public UInt64 countInser = 0;
        public UInt64 countDelete = 0;
        private Thread myThread;
        private MyPoint[,] matrix;
        public BlinkSky(Canvas BackgroundSky)
        {
            this.BackgroundSky = BackgroundSky;
            this.BackgroundSky.Background = Brushes.Black;
            matrix = new MyPoint[(int)this.BackgroundSky.Width, (int)this.BackgroundSky.Height];
        }
        public void ShowStar()
        {
            myThread = new Thread(ThreadDraw);
            myThread.Start();
        }
        private void ThreadDraw()
        {
            Random rand = new Random();
            while (!flagStop)
            {
                BackgroundSky.Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    int x = rand.Next(0, (int)BackgroundSky.Width - 1);
                    int y = rand.Next(0, (int)BackgroundSky.Height - 1);
                    if (Draw(BackgroundSky, x, y))
                    {
                        countInser++;
                    }
                    else
                    {
                         countDelete++;
                    }
                }));
                Thread.Sleep(1);

            }
        }
        private bool  Draw (Canvas canva, int x, int y)
        {
            if (matrix[x, y] == null)
            {
                MyPoint point = new MyPoint(x, y);
                matrix[x, y] = point;
                matrix[x, y].Draw(BackgroundSky);
                return true;
            }
            else
            {
                matrix[x, y].Remove(BackgroundSky);
                matrix[x, y] = null;
                return false;
            }
            
        }
        public double CalcStar()
        {
            return ((double)countDelete / (double)(countInser + 1));
        }
    }
}
