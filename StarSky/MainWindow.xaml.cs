﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StarSky
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BlinkSky sky;
        public MainWindow()
        {
            InitializeComponent();
            sky = new BlinkSky(CanvasSky);
            sky.ShowStar();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                sky.flagStop = true;
                double calc = sky.CalcStar();
                Label text = new Label();
                text.Content = "Процент: " + calc.ToString() + "%\n"+
                    "Число добавленных: " +sky.countInser.ToString() + "\n" +
                    "Число удаленных: " + sky.countDelete.ToString();
                Canvas.SetTop(text, CanvasSky.Height / 4);
                Canvas.SetLeft(text, CanvasSky.Width / 4);
                CanvasSky.Background = Brushes.White;
                CanvasSky.Children.Add(text);
            }
        }
    }
}
