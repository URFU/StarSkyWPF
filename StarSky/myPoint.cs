﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace StarSky
{
    class MyPoint
    {
        int x;
        int y;
        public Ellipse myEllipse;
        public MyPoint(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public void Draw(Canvas canvas)
        {
            myEllipse = new Ellipse();
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            Random rand = new Random();
            byte color1 = (byte)rand.Next(0, 0xFF);
            byte color2 = (byte)rand.Next(0, 0xFF);
            byte color3 = (byte)rand.Next(0, 0xFF);
            mySolidColorBrush.Color = Color.FromArgb(color1, color2, color3, 0);
            myEllipse.Fill = mySolidColorBrush;
            myEllipse.Width = 1;
            myEllipse.Height = 1;
            Canvas.SetTop(myEllipse, y);
            Canvas.SetLeft(myEllipse, x);
            canvas.Children.Add(myEllipse);

        }
        public void Remove(Canvas canvas)
        {
            canvas.Children.Remove(myEllipse);
        }
        private bool IsFreePosition(Canvas canvas, Ellipse myEllipse, int x, int y)    /*Место занято другой точкой? true - нет, false - да*/
        {
            return (canvas.Children.Contains(myEllipse));
        }
    }
}
